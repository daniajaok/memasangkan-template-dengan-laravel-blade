<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index','IndexController@index');
Route::post('/kirim','InputBiodataContrller@kirim');
Route::get('/form','InputBiodataContrller@form');
Route::get('/data-table',function(){
    return view('halaman.data-table');
    });
Route::get('/data-table2',function(){
    return view('halaman.data-table2');
    });
Route::get('/coba',function(){
    return view('layout.master');
    });