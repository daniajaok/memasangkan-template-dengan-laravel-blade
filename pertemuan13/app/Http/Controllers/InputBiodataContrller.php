<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InputBiodataContrller extends Controller
{
    //
    public function form(){
        return view('halaman.from');
    }
    public function kirim(Request $request){
        //dd($request->all());//pengetesan
        $namadepan=$request['fname'];
        $belakang=$request['lname'];

        return view('halaman.outputFrom',compact('namadepan','belakang'));
    }
}
