@extends('layout.master')

@section('judul')
Halaman form
@endsection

@section('content')
<form action="/kirim" method="post" >
  @csrf
  <label for="fname">First name:</label><br>
  <input type="text" id="fname" name="fname" value=""><br>
  <label for="lname">Last name:</label><br>
  <input type="text" id="lname" name="lname" value=""><br><br>
  <input type="submit" value="Submit">
  <!-- from.blade.php blade adalah mesin templateting -> {{}} sama seperti frame work lainya-->
</form>
@endsection 